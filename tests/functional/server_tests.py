'''
Integration/Functional tests for the solver.

These test cases require the existence of a Redis instance.
'''
import json, time, threading
import redis

from nose.tools import *
from unittest.mock import patch

from tsp_solver.server import Worker


class Worker_Test:
    def setup(self):
        self.worker = Worker()
        self.worker_thread = threading.Thread(target=self.worker.run)
        self.conn = redis.Redis(host='localhost')
        self.outbound = self.conn.pubsub(ignore_subscribe_messages=True)
        self.outbound.subscribe('tsp_solver.out')

    def teardown(self):
        self.conn.close()
        self.worker.stop()
        self.worker_thread.join(0.1)

    def receive_message(self, n=10):
        # try a few times to get a message from Redis, to overcome any delays
        for _ in range(n):
            msg = self.outbound.get_message()
            if msg and msg['type'] == 'message':
                return json.loads(msg['data'])
            time.sleep(0.1)
        return None

    def assert_outbound_message(self, message):
        received = self.receive_message()
        assert_dict_equal(received, message)

    def send_message(self, message, queue='tsp_solver.in'):
        self.conn.publish('tsp_solver.in', message)
        time.sleep(0.01)

    def send_messages_to_server_test(self):
        self.worker_thread.start()
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message('test-message')
        publish_method.assert_not_called()

    def server_accepts_only_JSON_payloads_test(self):
        self.worker_thread.start()
        payload = {
                'name': 'test',
                'data': [(1, 2), (2, 3), (3, 4)],
        }
        with patch.object(self.worker, '_process', return_value='success') as process_method:
            self.send_message(json.dumps(payload))
        process_method.assert_called_once_with({
            'name': 'test',
            'data': [[1, 2], [2, 3], [3, 4]],
        })

    def server_sends_result_to_outbound_queue_test(self):
        self.worker_thread.start()
        payload = {
                'name': 'test',
                'data': [(1, 2), (2, 3), (3, 4)],
        }
        self.send_message(json.dumps(payload))
        self.assert_outbound_message({
            'name': 'test',
            'result': [[0, 1, 2, 0]],
        })

    def server_ignores_messages_without_a_name_in_the_message_test(self):
        self.worker_thread.start()
        payload = {
                'data': [(1, 2), (2, 3), (3, 4)],
        }
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message(json.dumps(payload))
        publish_method.assert_not_called()

    def server_ignores_messages_without_data_in_the_message_test(self):
        self.worker_thread.start()
        payload = {'name': 'test'}
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message(json.dumps(payload))
        publish_method.assert_not_called()

    def server_ignores_messages_if_data_is_not_a_list_test(self):
        self.worker_thread.start()
        payload = {
                'name': 'test',
                'data': '0, 1, 2',
        }
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message(json.dumps(payload))
        publish_method.assert_not_called()

    def server_ignores_messages_if_data_is_not_a_list_test(self):
        self.worker_thread.start()
        payload = {
                'name': 'test',
                'data': '0, 1, 2',
        }
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message(json.dumps(payload))
        publish_method.assert_not_called()

    def server_ignores_messages_if_data_items_are_not_pairs_of_numbers_test(self):
        self.worker_thread.start()
        payload = {
                'name': 'test',
                'data': [(1, 1), '2, 2', [3, 3]],
        }
        with patch.object(self.worker, '_publish') as publish_method:
            self.send_message(json.dumps(payload))
        publish_method.assert_not_called()

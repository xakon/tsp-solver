'''
Unit-tests of creating data-structures to feed into OR-Tools.
'''
from nose.tools import *

from tsp_solver.solver import *


def euclidean_distance_tests():
    p, q = (236, 169), (228, 169)
    eq_(8, distance(p, q))


def extract_non_distance_data(data):
    return {
        'depot': data.get('depot'), 'vehicles': data.get('vehicles'),
    }

def create_data_model_with_valid_vehicles_test():
    vehicle_numbers = [1, 2, 10]
    expected_data = [
            {'depot': 0, 'vehicles': 1},
            {'depot': 0, 'vehicles': 2},
            {'depot': 0, 'vehicles': 10},
    ]

    for n, expected in zip(vehicle_numbers, expected_data):
        data = create_data_model([], n)
        compare = extract_non_distance_data(data)
        yield eq_, compare, expected

def create_data_model_with_invalid_vehicles_types_raises_ValueError_test():
    invalid_values = [
            None, 0, '0', -1,
    ]
    for n in invalid_values:
        with assert_raises(ValueError):
            create_data_model([], n)

def create_data_model_with_some_points_test():
    points = [
        (288, 149), (288, 129), (270, 133),
    ]
    data = create_data_model(points)
    assert_list_equal(data['distances'], [
        [ 0, 20, 24],
        [20,  0, 18],
        [24, 18,  0],
    ])

def calculate_distances_test():
    points = [
        (288, 149), (288, 129), (270, 133),
    ]
    result = calc_distances(points)
    assert_list_equal(result, [
        [ 0, 20, 24],
        [20,  0, 18],
        [24, 18,  0],
    ])

from nose.tools import *

from tsp_solver.solver import solution


def output_of_a_solution_test():
    result = solution([
        (288, 149), (288, 129), (270, 133),
    ])
    assert_list_equal(result, [
        [0, 1, 2, 0],
    ])

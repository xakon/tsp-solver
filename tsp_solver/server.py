'''
Service to solve in parallel the "Travelling Salesman Problem".
'''
import sys, json, logging

import redis

from tsp_solver import config, solver


class Worker:
    def __init__(self, host='', port=0, queue=''):
        host = host or config.HOST
        port = port or config.PORT
        queue = queue or config.QUEUE_INBOUND

        self.thread = None
        self.log = logging.getLogger('tsp_solver.server')
        self.conn = redis.Redis(host=host, port=port)
        self.channel = self.conn.pubsub(ignore_subscribe_messages=True)
        self._subscribe(queue)

    def run(self):
        self.thread = self.channel.run_in_thread(sleep_time=0.01)

    def stop(self):
        if self.thread:
            self.thread.stop()
        self.conn.close()

    def _subscribe(self, queue):
        self.channel.subscribe(**{queue: self._handler})

    def _handler(self, message):
        if message and message['type'] == 'message':
            self.log.debug('Received data message: %r', message)
            input_data = self._decode_data(message)
            result = self._process(input_data or {})
            if result:
                self._publish(result)

    def _decode_data(self, message):
        try:
            data = message.get('data', b'')
            return json.loads(data)
        except Exception as e:
            self.log.warn('Cannot decode message: %s', e)
            return None

    def _process(self, data):
        name = data.get('name')
        points = data.get('data')
        if not name or not self._validate(points):
            return None
        try:
            return {'name': name, 'result': solver.solution(points)}
        except Exception as e:
            self.log.warn('Error in processing data: %r: %s', data, e)
        return None

    def _validate(self, points):
        if points is None: return False
        if not isinstance(points, list): return False
        return True

    def _publish(self, data):
        self.conn.publish(config.QUEUE_OUTBOUND, json.dumps(data))


def main():
    worker = Worker()
    worker.run()

import math

from ortools.constraint_solver import pywrapcp, routing_enums_pb2


def solution(points, vehicles=1):
    def distance_callback(from_index, to_index):
        'Return the distance between 2 nodes.'
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distances'][from_node][to_node]

    data = create_data_model(points, vehicles)
    distances, depot = data['distances'], data['depot']
    manager = pywrapcp.RoutingIndexManager(len(distances), vehicles, depot)
    routing = pywrapcp.RoutingModel(manager)

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC
    )

    result = routing.SolveWithParameters(search_parameters)
    if not result:
        return []
    return get_routes(result, routing, manager)


def get_routes(solution, routing, manager):
    """
    Get vehicle routes from a solution and store them in an array.

    Get vehicle routes and store them in a 2D array whose i,j entry is the jth
    location visited by vehicle i along its route.
    """
    routes = []
    for route_nbr in range(routing.vehicles()):
        index = routing.Start(route_nbr)
        route = [manager.IndexToNode(index)]
        while not routing.IsEnd(index):
            index = solution.Value(routing.NextVar(index))
            route.append(manager.IndexToNode(index))
        routes.append(route)
    return routes


def distance(p, q) -> int:
    'Calculate the Euclidean Distance of 2 points.'
    a = p[0] - q[0]
    b = p[1] - q[1]
    return int(math.hypot(a, b))


def create_data_model(points, vehicles=1) -> dict:
    '''
    Create the data model to feed into OR-Tools Routing Manager.
    '''
    if not isinstance(vehicles, int) or vehicles < 1:
        raise ValueError('Invalid vehicles number: %r' % vehicles)
    return {
        'depot': 0,
        'vehicles': vehicles,
        'distances': calc_distances(points),
    }

def calc_distances(points):
    points = list(points)
    matrix = []
    for i, p in enumerate(points):
        matrix.append([
            0 if i == j else distance(p, q)
                for j, q in enumerate(points)
        ])
    return matrix

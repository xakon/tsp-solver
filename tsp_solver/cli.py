'''
Run a server to solve the "Travailing Salesman Problem".

The server waits for input from a Redis inbound queue and writes the output
into another Redis queue.
'''
import argparse

from . import config


def create_parser():
    parser = argparse.ArgumentParser(prog='tsp-solver', description=__doc__)
    parser.add_argument(
            '-v', '--version',
            action='version',
            version='%(prog)s v' + config.VERSION,
    )
    parser.add_argument(
            '-r', '--host',
            default=config.HOST,
            help='Hostname of Redis server (default: %(default)r)',
    )
    parser.add_argument(
            '-p', '--port',
            default=config.PORT,
            help='Port the Redis server listens at (default: %(default)s)',
    )
    parser.add_argument(
            '-n', '--instances',
            default=1, type=int,
            help='How many workers the server will launch (default: %(default)s)',
    )
    parser.add_argument(
            '-l', '--log-level',
            default='INFO', choices=[
                'DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL',
            ],
            help='Set log level (default: %(default)s)',
    )
    return parser

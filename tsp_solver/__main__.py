import signal, logging

from multiprocessing import Process

from .cli import create_parser
from .server import Worker


def signal_handler(signum, stack_frame):
    worker.stop()


parser = create_parser()
options = parser.parse_args()

logging.basicConfig()
logger = logging.getLogger('tsp_solver')
logger.setLevel(options.log_level.upper())

signal.signal(signal.SIGINT, signal_handler)
params = {
        'host': options.host,
        'port': options.port,
}
worker = Worker(**params)
processes = [
    Process(target=worker.run)
        for _ in range(options.instances)
]
for p in processes:
    p.start()
for p in processes:
    p.join()

'''
Default configuration options for server.
'''

VERSION = '1.0'

HOST = 'localhost'
PORT = 6379

QUEUE_INBOUND = 'tsp_solver.in'
QUEUE_OUTBOUND = 'tsp_solver.out'

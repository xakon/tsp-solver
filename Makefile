##
## Makefile
## Manage project
##

SHELL		:= /bin/sh


all: help
help:
	@echo 'Build and manage local project'
clean:
	poetry env remove
build:
	poetry install
run-server:
	poetry run ./server


.PHONY: all help build clean run-server

Travelling Salesman Problem Solver
==================================

A Python service that solves the [Travelling Salesman Problem][wikipedia-tsr]
using the [Google OR-Tools][or-tools].


Description
-----------

The server runs in the background and waits for messages to arrive over a
[Redis][redis] inbound queue (by default, `tsp_solver.in`).  It calculates
the possible answers to the given input for a **single** vehicle that needs
to travel over the given input locations, which are expressed as *N* geocoded
points.  The result is sent over [Redis][redis] to another outbound queue
(by default, `tsp_solver.out`).

The user of the server is able to determine how many worker processes the server
will create in order to server the incoming messages.

For a complete list of supported options, the user can type:

    $ ./server -h


Development
-----------

We can start working with the project either by using [poetry][poetry] or using
`pip` and/or Python's virtual environment.  In the first case, we can create a
virtual environment and install all the dependencies by issuing:

    $ make build

We can run the server by running:

    $ make run-server   # or ./server

We can use a local demo client to validate the server's operation:

    $ poetry run python demo.py


Internals
---------

The format of the incoming messages should be a JSON document of the form:

```
{
   "name": "string",
   "data": [
      [34.5343, -12.4343], [-81.3435, 44.54343], ...
   ]
}
```

The `data` field contains the *N* lat-lon geocoded points the vehicle needs to
visit.

The output messages are again JSON encoded messages of the form:

```
{
   "name": "string",
   "result": [
      [0, 1, 2, 0],
      [0, 2, 1, 0],
   ]
}
```

Each index in the arrays of the `result` is the location the vehicle needs to
visit first.


[wikipedia-tsr]:	https://en.wikipedia.org/wiki/Travelling_salesman_problem
[or-tools]:		https://developers.google.com/optimization/
[redis]:		https://redis.io/
[poetry]:		https://python-poetry.org/

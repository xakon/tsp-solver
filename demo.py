'''
A basic client to demonstrate server's operation.

Create some random messages and send them to the server using Redis.
At the same time, print asynchronously the processed results.
'''
import json, random, signal, threading
import redis

from tsp_solver import config


def print_message(message):
    if message and message['type'] == 'message':
        data = message['data']
        d = json.loads(data)
        print(f"{d['name']}: {d['result']}")

def random_point():
    from random import uniform

    lat = uniform(-90, 90.1)
    lon = uniform(-180, 180.1)
    lat = 90.0 if lat > 90.0 else lat
    lon = 180.0 if lon > 180.0 else lon
    return (lat, lon)

def random_points(N=12):
    n = random.randint(1, N)
    return [random_point() for _ in range(n)]

def signal_handler(signum, stack_frame):
    stop_event.set()
    if thread:
        thread.stop()
    thread.join(1)
    r.close()

stop_event = threading.Event()
signal.signal(signal.SIGINT, signal_handler)
random.seed()

r = redis.Redis(host='localhost')
p = r.pubsub(ignore_subscribe_messages=True)
p.subscribe(**{config.QUEUE_OUTBOUND: print_message})
thread = p.run_in_thread(sleep_time=0.1)

counter = 0
message_name = 'Test #%d'
while not stop_event.is_set():
    message = {
        'name': message_name % counter,
        'data': random_points(),
    }
    r.publish(config.QUEUE_INBOUND, json.dumps(message))
    counter += 1
    stop_event.wait(random.random())
